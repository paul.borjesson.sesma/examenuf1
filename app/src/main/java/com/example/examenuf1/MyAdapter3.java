package com.example.examenuf1;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter3 extends RecyclerView.Adapter<MyAdapter3.MyViewHolder>{

    private ArrayList<Song> songs;
    private Context mContext;
    boolean check = true;
    AnimatorSet a = new AnimatorSet();

    public MyAdapter3(ArrayList<Song> songs, Context mContext) {
        this.songs = songs;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyAdapter3.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.my_vertical, parent, false );
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter3.MyViewHolder holder, int position) {
        Picasso.get().load(songs.get(position).getBandSong())
                .fit()
                .centerCrop()
                .into(holder.imageCD);
        holder.titleSong.setText(songs.get(position).getNameSong());
        holder.bandName.setText(songs.get(position).getImageSong());

        //Fent click a la imatge
        holder.imageCD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SongActivity.class);
                intent.putExtra("imageSong", songs.get(holder.getAdapterPosition()).getImageSong());
                intent.putExtra("nameSong", songs.get(holder.getAdapterPosition()).getNameSong());
                intent.putExtra("bandSong", songs.get(holder.getAdapterPosition()).getBandSong());
                intent.putExtra("year", songs.get(holder.getAdapterPosition()).getYearSong());
                intent.putExtra("lyrics", songs.get(holder.getAdapterPosition()).getLyrics());

                mContext.startActivity(intent);
            }
        });

        //Fent click al text
        holder.titleSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SongActivity.class);
                intent.putExtra("imageSong", songs.get(holder.getAdapterPosition()).getImageSong());
                intent.putExtra("nameSong", songs.get(holder.getAdapterPosition()).getNameSong());
                intent.putExtra("bandSong", songs.get(holder.getAdapterPosition()).getBandSong());
                intent.putExtra("year", songs.get(holder.getAdapterPosition()).getYearSong());
                intent.putExtra("lyrics", songs.get(holder.getAdapterPosition()).getLyrics());

                mContext.startActivity(intent);
            }
        });

        //Fent click al botó
        holder.playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check) {
                    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(holder.playButton, "rotation", 360f, 0f).setDuration(1000);
                    holder.playButton.setImageResource(R.drawable.play2);
                    a.play(objectAnimator);
                    a.start();
                    check = false;
                } else {
                    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(holder.playButton, "rotation", 0f, 720f).setDuration(1000);
                    holder.playButton.setImageResource(R.drawable.play);
                    a.play(objectAnimator);
                    a.start();
                    check = true;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return songs.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageCD;
        private TextView titleSong;
        private TextView bandName;
        private ImageView playButton;

        ConstraintLayout my_vertical;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageCD = itemView.findViewById(R.id.imageCD);
            titleSong = itemView.findViewById(R.id.titleSong);
            bandName = itemView.findViewById(R.id.bandName);
            my_vertical = itemView.findViewById(R.id.myVertical);

            playButton = itemView.findViewById(R.id.playButton);
        }
    }
}
