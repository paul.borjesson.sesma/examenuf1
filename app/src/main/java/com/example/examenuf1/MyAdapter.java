package com.example.examenuf1;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private ArrayList<CD> cds;
    private Context mContext;

    public MyAdapter(ArrayList<CD> cds, Context mContext) {
        this.cds = cds;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.my_horizontal, parent, false );
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
        Picasso.get().load(cds.get(position).getBandCD()).fit().centerCrop().into(holder.imageView);
        holder.textView.setText(cds.get(position).getNameCD());

        holder.myHorizontal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CdActivity.class);
                intent.putExtra("nameCD", cds.get(holder.getAdapterPosition()).getNameCD());
                intent.putExtra("bandCD", cds.get(holder.getAdapterPosition()).getBandCD());
                intent.putExtra("imageCD",cds.get(holder.getAdapterPosition()).getImageCD());
                intent.putExtra("infoCD", cds.get(holder.getAdapterPosition()).getInfoCD());
                intent.putExtra("songs", cds.get(holder.getAdapterPosition()).getSongs());

                mContext.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return cds.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textView;

        ConstraintLayout myHorizontal;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.textView);

            myHorizontal = itemView.findViewById(R.id.myHorizontal);
        }
    }
}
