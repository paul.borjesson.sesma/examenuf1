package com.example.examenuf1;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

public class SongActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView titleSongCD;
    private TextView titleBandCD;
    private TextView year;
    private TextView textLyricsLetters;

//    AnimatorSet a = new AnimatorSet();
//    private ImageView playButton;
//    boolean check = true;

    String imageSong, nameSong, bandSong, yearNum, lyrics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song);

        //Hooks
        imageView = findViewById(R.id.imageView);
        titleSongCD = findViewById(R.id.titleSongCD);
        titleBandCD = findViewById(R.id.titleBandCD);
        year = findViewById(R.id.year);
        textLyricsLetters = findViewById(R.id.textLyricsLetters);

        //playButton = findViewById(R.id.playButton);

        getData();
        setData();

//        playButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (check) {
//                    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(playButton, "rotation", 360f, 0f).setDuration(1000);
//                    playButton.setImageResource(R.drawable.play2);
//                    a.play(objectAnimator);
//                    a.start();
//                    check = false;
//                } else {
//                    ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(playButton, "rotation", 0f, 720f).setDuration(1000);
//                    playButton.setImageResource(R.drawable.play);
//                    a.play(objectAnimator);
//                    a.start();
//                    check = true;
//                }
//            }
//        });

    }

    private void getData() {
        if (getIntent().hasExtra("imageSong") &&
                getIntent().hasExtra("nameSong")) {
            imageSong = getIntent().getStringExtra("imageSong");
            nameSong = getIntent().getStringExtra("nameSong");
            bandSong = getIntent().getStringExtra("bandSong");
            yearNum = getIntent().getStringExtra("year");
            lyrics = getIntent().getStringExtra("lyrics");
        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData() {
        Picasso.get().load(bandSong)
                .fit()
                .centerCrop()
                .into(imageView);
        titleSongCD.setText(nameSong);
        titleBandCD.setText(imageSong);
        year.setText(yearNum);
        textLyricsLetters.setText(lyrics);
    }
}