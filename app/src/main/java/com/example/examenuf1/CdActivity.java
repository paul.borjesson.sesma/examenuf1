package com.example.examenuf1;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CdActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView nameCd;
    private TextView nameBand;
    private TextView bandInfo;
    private RecyclerView rvSongs;
    private ArrayList<Song> songs;

    String nameCD, bandCD, imageCD, infoCD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cd);
        getSupportActionBar().hide();

        //Hooks
        imageView = findViewById(R.id.imageView);
        nameCd = findViewById(R.id.nameCd);
        nameBand = findViewById(R.id.nameBand);
        bandInfo = findViewById(R.id.bandinfo);
        rvSongs = findViewById(R.id.rvSongs);

        getData();
        setData();

        MyAdapter3 myAdapter3 = new MyAdapter3(songs, this);
        rvSongs.setAdapter(myAdapter3);
        rvSongs.setLayoutManager(new LinearLayoutManager(this));
    }

    private void getData() {
        if (getIntent().hasExtra("nameCD") &&
                getIntent().hasExtra("bandCD")) {
                nameCD = getIntent().getStringExtra("nameCD");
                bandCD = getIntent().getStringExtra("bandCD");
                imageCD = getIntent().getStringExtra("imageCD");
                infoCD = getIntent().getStringExtra("infoCD");
                songs = (ArrayList<Song>) getIntent().getSerializableExtra("songs");
        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData() {
        nameCd.setText(nameCD);
        Picasso.get().load(bandCD)
                .fit()
                .centerCrop()
                .into(imageView);
        bandInfo.setText(infoCD);
        nameBand.setText(imageCD);
    }

}